﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Example
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string conn = "Data Source=(localdb)\\dbExampleADO;Initial Catalog=ADO;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            using (SqlConnection c = new SqlConnection(conn))
            {
                Console.WriteLine("Connected mode");
                using(SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT Id, lastName, firstname FROM v_student;
                    ";

                    c.Open();
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine($"{reader["Id"]} {reader["lastName"]}, {reader["firstName"]}");
                        }
                    }

                    c.Close();
                }

                Console.WriteLine("Disconnected mode");

                using(SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT lastName, firstname FROM student;
                    ";

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();

                    da.Fill(ds);

                    if(ds.Tables.Count > 0)
                    {
                        foreach(DataRow dr in ds.Tables[0].Rows)
                        {
                            Console.WriteLine($"{dr["lastName"]}, {dr["firstName"]}");
                        }
                    }
                }

                Console.WriteLine("Connected mode");
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT AVG(YearResult) FROM student;
                    ";

                    c.Open();
                    int yearResult = (int)cmd.ExecuteScalar();

                    c.Close();

                    Console.WriteLine($"{yearResult}");
                }

            }
            Console.ReadLine();
        }
    }
}
